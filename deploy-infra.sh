#!/bin/sh
set -euf

ENVIRONMENT="test"
export AWS_PROFILE="quiet-tusky-${ENVIRONMENT}-admin"
DEPLOYMENT_BUCKET="quiet-tusky-${ENVIRONMENT}-deploy-ca-central-1"

#./ Update the infrastructure
aws cloudformation update-stack \
  --stack-name "QuietTusky-${ENVIRONMENT}" \
  --capabilities CAPABILITY_NAMED_IAM \
  --template-body file://cfn/template.yml 

