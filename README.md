# QuietTusky

## What is this?

This lets you block a Mastodon account and all of their followers.

## How do I run it?

You'll need [Python](https://www.python.org/).

Then, you can run:

```
python3 quiet-tusky.py --username USER@INSTANCE
```

This will make a file, `blocklist.csv`. You can upload this using the import
feature on your instance.

## Does it always work?

TK


