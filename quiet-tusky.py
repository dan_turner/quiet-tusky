import argparse
import urllib.request
import json
import doctest



def get_username_from_url(user_url):
    return user_url.split("/")[4]


def get_instance_from_url(user_url):
    return user_url.split("/")[2]


def get_followers(username, instance, page):
    target_url = "https://{}/users/{}/followers.json?page={}".format(
        instance, username, page)
    print(target_url)
    r = urllib.request.urlopen(target_url)
    
    return json.loads(r.read())["orderedItems"]


def parse_args():
    parser = argparse.ArgumentParser(prog="quiet-tusky",
        description="Generates a blocklist from a mastodon users' followers")
    parser.add_argument("--username", type=str, required=True,
        help="Format is User@Instance, no leading '@'")
    parser.add_argument("--start-at-page", type=int, default=1,
       help="If one run crashed you can start from the last successful page.")
    parser.add_argument("--blockfile", type=str, default="blocklist.csv")
    return parser.parse_args()
    
    
def main():
    args = parse_args()
    target_user_name = args.username.split("@")[0]
    target_instance = args.username.split("@")[1]

    output_file = args.blockfile
    
    with open(output_file, 'a') as blocklist:
        current_page = args.start_at_page
        followers_list = get_followers(target_user_name, target_instance, current_page)
        blocklist.write("{}@{}\n".format(target_user_name, target_instance))
        while followers_list != []:
            for follower in followers_list:
                user = get_username_from_url(follower)
                instance = get_instance_from_url(follower)
                blocklist.write("{}@{}".format(user, instance))
                blocklist.write('\n')
            current_page += 1
            followers_list = get_followers(target_user_name, target_instance, current_page)
            
            
if __name__ == "__main__":
    main()