#!/bin/sh
set -euf

ENVIRONMENT="test"
export AWS_PROFILE="quiet-tusky-${ENVIRONMENT}-admin"
DEPLOYMENT_BUCKET="quiet-tusky-${ENVIRONMENT}-deploy-ca-central-1"

aws s3 sync pages s3://quiet-tusky-spa-test-ca-central-1/
 
aws cloudfront create-invalidation --distribution-id EM9H4NY0A14RV --paths "/index.html"

