#!/bin/sh
set -euf

ENVIRONMENT="test"
export AWS_PROFILE="quiet-tusky-${ENVIRONMENT}-admin"
DEPLOYMENT_BUCKET="quiet-tusky-${ENVIRONMENT}-deploy-ca-central-1"

# Create a deployment package
cd backend/
zip -r backend.zip index.py

# Update our Lambda
aws s3 cp \
  backend.zip "s3://${DEPLOYMENT_BUCKET}/backend.zip"
aws lambda update-function-code \
  --function-name "QuietTusky-${ENVIRONMENT}" \
  --s3-bucket "${DEPLOYMENT_BUCKET}" \
  --s3-key backend.zip
  
cd -
