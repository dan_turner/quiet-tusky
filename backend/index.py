import base64
import logging
import urllib.request
import json

logger = logging.getLogger()
logger.setLevel(logging.INFO)



def get_followers(username, instance, page):
    target_url = "https://{}/users/{}/followers.json?page={}".format(
        instance, username, page)
    print(target_url)
    r = urllib.request.urlopen(target_url)
    
    return json.loads(r.read())["orderedItems"]


def lambda_handler(event, context):
    print(event)
    # https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-cors.html
    target_user = event["headers"]["target-user"].strip("\"")
    page = event["headers"]["page"]

    target_username = target_user.split('@')[0]
    target_instance = target_user.split('@')[1]

    return {
        'statusCode': 200,
        'headers': {
                'Access-Control-Allow-Headers': "Content-Type,Target-User,Page",
                'Access-Control-Allow-Origin': "https://d1m236g9h1xc1j.cloudfront.net",
                'Access-Control-Allow-Methods': "GET"
                },
        'body': json.dumps(get_followers(target_username, target_instance, page))
    }
