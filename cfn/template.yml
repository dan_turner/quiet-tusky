
AWSTemplateFormatVersion: "2010-09-09"

Description: QuietTusky stack
Parameters:
  Environment:
    Type: String
    Default: test
Resources:
  LambdaFunction:
    Type: 'AWS::Lambda::Function'
    Properties:
      FunctionName: !Sub "QuietTusky-${Environment}"
      Handler: index.lambda_handler
      Runtime: python3.7
      Role: !GetAtt LambdaFunctionRole.Arn
      MemorySize: 128
      Timeout: 2
      Code:
         S3Bucket: !Sub "quiet-tusky-${Environment}-deploy-ca-central-1"
         S3Key: backend.zip
  LambdaFunctionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Effect: Allow
          Principal:
            Service:
            - lambda.amazonaws.com
          Action:
          - sts:AssumeRole
      Path: "/"
      Policies:
      - PolicyName: AppendToLogsPolicy
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - logs:CreateLogGroup
            - logs:CreateLogStream
            - logs:PutLogEvents
            Resource: "*"
      
  ApiEndpoint:
    Type: 'AWS::ApiGatewayV2::Api'
    Properties:
      Name: "QuietTusky"
      Description: "QuietTusky public API"
      ProtocolType: "HTTP"
  
  ApiIntegration:
    Type: AWS::ApiGatewayV2::Integration
    Properties: 
      ApiId: !Ref ApiEndpoint
      IntegrationType: "AWS_PROXY"
      IntegrationUri: !Join
        - ''
        - - 'arn:'
          - !Ref 'AWS::Partition'
          - ':apigateway:'
          - !Ref 'AWS::Region'
          - ':lambda:path/2015-03-31/functions/'
          - !GetAtt LambdaFunction.Arn
          - /invocations
      IntegrationMethod: POST
      PayloadFormatVersion: '2.0'
      TimeoutInMillis: 2100

  ApiRoute:
    Type: AWS::ApiGatewayV2::Route
    DependsOn:
      - ApiIntegration
    Properties:
      ApiId: !Ref ApiEndpoint
      AuthorizationType: NONE
      RouteKey: 'GET /user'
      Target: !Join
        - /
        - - integrations
          - !Ref ApiIntegration

  CorsRoute:
    Type: AWS::ApiGatewayV2::Route
    DependsOn:
      - ApiIntegration
    Properties:
      ApiId: !Ref ApiEndpoint
      AuthorizationType: NONE
      RouteKey: 'OPTIONS /user'
      Target: !Join
        - /
        - - integrations
          - !Ref ApiIntegration
      
  LambdaApiGatewayPermission:
    Type: AWS::Lambda::Permission
    Properties:
      Action: lambda:InvokeFunction
      FunctionName: !GetAtt LambdaFunction.Arn
      Principal: apigateway.amazonaws.com
      SourceArn: !Sub arn:aws:execute-api:${AWS::Region}:${AWS::AccountId}:${ApiEndpoint}/*

  ApiLogGroup: 
    Type: AWS::Logs::LogGroup
    Properties: 
      LogGroupName: !Sub "quiet-tusky-api-logs-${Environment}"
      RetentionInDays: 1827
  
  ApiStage:
    Type: 'AWS::ApiGatewayV2::Stage'
    DependsOn:
      - ApiEndpoint
    Properties:
      StageName: "$default"
      Description: !Sub "${Environment} Stage"
      AutoDeploy: true
      ApiId: !Ref ApiEndpoint
      DefaultRouteSettings:
        DetailedMetricsEnabled: true
        DataTraceEnabled: false
        ThrottlingBurstLimit: 3
        ThrottlingRateLimit: 1
      AccessLogSettings:
        DestinationArn: !GetAtt ApiLogGroup.Arn
        Format: >-
          {"requestId":"$context.requestId", "ip": "$context.identity.sourceIp",
          "caller":"$context.identity.caller", "user":"$context.identity.user",
          "requestTime":"$context.requestTime", "routeKey":"$context.routeKey",
          "status":"$context.status"}
          
  
  QuietTuskyCloudFrontIdentity:
    Type: AWS::CloudFront::CloudFrontOriginAccessIdentity
    Properties:
      CloudFrontOriginAccessIdentityConfig:
        Comment: !Sub "QuietTusky (${Environment}) Origin Access Identity"
        
  QuietTuskySpaS3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Sub "quiet-tusky-spa-${Environment}-${AWS::Region}"
  
  # Allow CloudFormation to ready index.html and 404.html from our S3 bucket.
  QuietTuskySpaS3BucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: !Ref QuietTuskySpaS3Bucket
      PolicyDocument:
        Statement:
          - Action:
              - "s3:GetObject"
            Effect: Allow
            Principal:
              AWS:
                !Join [
                  "",
                  [
                    "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ",
                    !Ref QuietTuskyCloudFrontIdentity,
                  ],
                ]
            Resource: !Join ["", ["arn:aws:s3:::", !Ref QuietTuskySpaS3Bucket, "/*"]]
        Version: "2012-10-17"
  
  QuietTuskyCloudFront:
    Type: "AWS::CloudFront::Distribution"
    Properties:
      # https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-cloudfront-distribution-distributionconfig.html
      DistributionConfig:
        #Aliases:
        #  - !FindInMap [EnvironmentMaps, !Ref Env, "Domain"]
        Comment: !Ref Environment
        # This must be defined, even with all default values. Altough values are
        # a list of string, only certain combinations are allowed. Please check
        # the documentation for more details
        #
        # https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-cloudfront-distribution-defaultcachebehavior.html
        DefaultCacheBehavior:
          AllowedMethods:
            - GET
            - HEAD
          CachedMethods:
            - GET
            - HEAD
          # Required field
          ForwardedValues:
            QueryString: True
          # This value must match the Origin.Id defined below, under the
          # Origins key
          TargetOriginId:
            !Sub "quiet-tusky-spa-${Environment}-${AWS::Region}"
          # Super nice automatic redirection of HTTP to HTTPS
          ViewerProtocolPolicy: redirect-to-https
        # This is required, even if not mentioned in the AWS documentation. For
        # some reason, SPA will not work properly if CloudFront does not have a
        # default entry point
        DefaultRootObject: index.html
        # Enabled CloudFront as soon as created
        Enabled: True
        HttpVersion: http2
        IPV6Enabled: True
        # https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-cloudfront-distribution-origin.html
        Origins:
            # Use the regional domain name instead of the global name 
            # ( {bucket}.s3.amazonaws.com )
            # If the global name is used, the CloudFront URL will only redirect the
            # requests to S3 global domain name instead of serving the content. As
            # S3 content is only available to CloudFront, not to the public access,
            # such request ends up with a 403 enrror
            #
            # https://stackoverflow.com/a/58423033/4906586
          - DomainName: !GetAtt QuietTuskySpaS3Bucket.RegionalDomainName
            Id: !Sub "quiet-tusky-spa-${Environment}-${AWS::Region}"
            # S3 bucket access restriction: ensure that content is only available
            # through CloudFront
            #
            # https://docs.aws.amazon.com/cloudfront/latest/APIReference/API_S3OriginConfig.html
            S3OriginConfig:
              OriginAccessIdentity: !Sub "origin-access-identity/cloudfront/${QuietTuskyCloudFrontIdentity}"
        PriceClass: "PriceClass_100"
        # https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-cloudfront-distribution-viewercertificate.html
        #ViewerCertificate:
          # If the certificate is part of the CloudFormation stack, use its reference
          # instead:
          #   !Ref <CERTIFICATE LOGICAL ID>
        #  AcmCertificateArn: !Ref AwsCertificateArn
        #  MinimumProtocolVersion: TLSv1.2_2018 # recommended value if there is no browser support issue
        #  SslSupportMethod: sni-only
        
Outputs:
  CloudFrontUrl:
    Value:        !GetAtt QuietTuskyCloudFront.DomainName
    Description:  "QuietTuskyCloudFront URL"
  ApiUrl:
    Value:  !GetAtt ApiEndpoint.ApiEndpoint
    Description:  "QuietTusky API Url"